import { z } from "zod";

import { ZUser } from "../users/user.types";

export const ZCredentials = ZUser.pick({ username: true, password: true });
export interface ICredentials extends z.infer<typeof ZCredentials> {}

export const ZSignupData = ZUser.pick({ username: true, password: true, role: true });
export interface SignupData extends z.infer<typeof ZSignupData> {}
