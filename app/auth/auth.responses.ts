import { IResponses } from "../utils/base-schema";

export const authResponses: IResponses = {
	USER_ALREADY_REGISTERED: {
		statusCode: 403,
		message: "AUTH: USER ALREADY REGISTERED",
	},
	INVALID_CREDENTIALS: {
		statusCode: 403,
		message: "AUTH: INVALID CREDENTIALS",
	},
	REGISTRATION_FAILED: {
		statusCode: 400,
		message: "AUTH: REGISTRATION FAILED",
	},
	SERVER_ERR: {
		statusCode: 500,
		message: "AUTH: SERVER ERR",
	},
};
