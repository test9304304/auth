import { match } from "path-to-regexp";

import userRoutes from "../users/user.routes";
import authRoutes from "../auth/auth.routes";

import { ExcludedRoutes, Route } from "./routes.types";

export const routes: Route[] = [userRoutes, authRoutes];

export const excludedRoutes: ExcludedRoutes = [
	{ path: match("/auth/login"), method: "POST" },
	{ path: match("/auth/signup"), method: "POST" },
];
